package com.edugaon.api

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

class HomePageActivity : ApiApplication() {
    lateinit var text : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)
        dataGetOnServer()
      text = findViewById(R.id.text1)
    }

    public fun dataGetOnServer(){
        val str = JsonObjectRequest(
            Request.Method.GET , dburl+"getdata.php" , null,
            {
                try {
                    val bol = it.getString("userName")
//                    val kum = it.getString("kumar")
                    text.text = bol.toString()
//                    if(bol){
                    val sf  = bol.toString()
                    Toast.makeText(applicationContext, "success $sf ", Toast.LENGTH_SHORT).show()
//                    }
//                    txt.text = bol.toString()
                }
                catch (e:Exception){
                    Log.d("Massage","Not catch")
                    Toast.makeText(applicationContext, "catch", Toast.LENGTH_SHORT).show()
                }
            }, {
                Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
            }
        )
        Volley.newRequestQueue(this).add(str)
    }

}