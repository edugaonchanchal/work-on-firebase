package com.edugaon.api

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

class SplashActivity : ApiApplication() {
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        saveData()

//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
//        editor  = sharedPreferences.edit()

        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        pref.apply {
            val name = getString("NAME", "")
            val id = getString("id", "")
            if (name == "CHANCDHCAL" && id == "1234") {
                Toast.makeText(
                    applicationContext,
                    "True Store + $name + = + $id",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    public fun saveData() {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = pref.edit()

        editor
            .putString("NAME", "CHANCDHCAL")
            .putString("id", "1234")
            .apply()
    }

    public fun dataGetOnServer() {
        val str = JsonObjectRequest(
            Request.Method.GET, dburl + "getdata.php", null,
            {
                try {
                    val bol = it.getString("userName")
                    val sf = bol.toString()
                    Toast.makeText(applicationContext, "success $sf ", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    Log.d("Massage", "Not catch")
                    Toast.makeText(applicationContext, "catch", Toast.LENGTH_SHORT).show()
                }
            }, {
                Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
            }
        )
        Volley.newRequestQueue(this).add(str)
    }


//    yes,  I am doing in your company. I will doing work in your company full time , you give  me opportunity , I gave a beater experience in your company. I have  more time for our company

//    I am Android developer and your need good Android developer. I have 8 months expriencs, i am done this project - Cashbank  and metzee. I have Knowledge kotlin, firebase, rest api by volley, HTML, bootstrap, CSS, PHP, javascript


}